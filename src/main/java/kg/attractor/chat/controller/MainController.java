package kg.attractor.chat.controller;

import kg.attractor.chat.model.ChatSession;
import kg.attractor.chat.model.Customer;
import kg.attractor.chat.model.CustomerRegisterForm;
import kg.attractor.chat.model.Message;
import kg.attractor.chat.repository.CustomerRepository;
import kg.attractor.chat.service.ChatSessionService;
import kg.attractor.chat.service.CustomerService;
import kg.attractor.chat.service.PropertiesService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@RequestMapping("/main")
public class MainController {
    private final CustomerService customerService;
    private final CustomerRepository cr;
    private final PropertiesService propertiesService;
    private final ChatSessionService chatSessionService;

    @GetMapping
    public String mainPage() {
        return "main";
    }

    @GetMapping("/registration")
    public String pageRegisterCustomer(Model model) {
        if (!model.containsAttribute("dto")) {
            model.addAttribute("dto", new CustomerRegisterForm());
        }
        return "registration";
    }

    @PostMapping("/registration")
    public String registerPage(@Valid CustomerRegisterForm customerRequestDto,
                               BindingResult validationResult,
                               RedirectAttributes attributes) {
        attributes.addFlashAttribute("dto", customerRequestDto);

        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/main/registration";
        }

        customerService.register(customerRequestDto);
        return "redirect:/main/login";
    }

    @GetMapping("/login")
    public String loginPage(@RequestParam(required = false, defaultValue = "false") Boolean error, Model model) {
        model.addAttribute("error", error);
        return "login";
    }
    @GetMapping("/logout")
    public String invalidate(HttpSession session) {
        if (session != null) {
            session.invalidate();
        }
        return "redirect:/main";
    }
    private static <T> void constructPageable(Page<T> list, int pageSize, Model model, String uri) {
        if (list.hasNext()) {
            model.addAttribute("nextPageLink", constructPageUri(uri, list.nextPageable().getPageNumber(), list.nextPageable().getPageSize()));
        }

        if (list.hasPrevious()) {
            model.addAttribute("prevPageLink", constructPageUri(uri, list.previousPageable().getPageNumber(), list.previousPageable().getPageSize()));
        }

        model.addAttribute("hasNext", list.hasNext());
        model.addAttribute("hasPrev", list.hasPrevious());
        model.addAttribute("items", list.getContent());
        model.addAttribute("defaultPageSize", pageSize);
    }
    private static String constructPageUri(String uri, int page, int size) {
        return String.format("%s?page=%s&size=%s", uri, page, size);
    }
    @GetMapping("/chats")
    public String getChats(Model model, Pageable pageable, HttpServletRequest uriBuilder){
        var users = customerService.getCustomers(pageable);
        var uri = uriBuilder.getRequestURI();
        constructPageable(users, propertiesService.getDefaultPageSize(),model,uri);
        return "chats";
    }
    @GetMapping("/chat/{id}")
    public String getChat(@PathVariable String id, Model model,HttpServletRequest uriBuilder,Pageable pageable,Authentication authentication){
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var user = cr.findByEmail(userDetails.getUsername());
        var session = chatSessionService.getSession(pageable, id,authentication);
        var uri = uriBuilder.getRequestURI();
        constructPageable(session,propertiesService.getDefaultPageSize(),model,uri);
        model.addAttribute("user",user);
        return "chat";
    }

    @RequestMapping(value = "/chat/{id}", method = RequestMethod.POST)
    public void createMessage(@PathVariable String id, @RequestParam("message") String message, Authentication authentication){
        chatSessionService.addMessage(message, id, authentication);
    }
    @GetMapping("/messages/{id}")
    @ResponseBody
    public Message returnMessages(@PathVariable int id){
        return chatSessionService.getMessage(id);
    }
}
