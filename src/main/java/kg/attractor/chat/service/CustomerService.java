package kg.attractor.chat.service;

import kg.attractor.chat.DTO.CustomerDTO;
import kg.attractor.chat.DTO.CustomerResponseDTO;
import kg.attractor.chat.exception.CustomerAlreadyRegisteredException;
import kg.attractor.chat.model.Customer;
import kg.attractor.chat.model.CustomerRegisterForm;
import kg.attractor.chat.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CustomerService {

    private final CustomerRepository repository;
    private final PasswordEncoder encoder;

    public CustomerResponseDTO register(CustomerRegisterForm form) {
        if (repository.existsByEmail(form.getEmail())) {
            throw new CustomerAlreadyRegisteredException();
        }

        var user = Customer.builder()
                .email(form.getEmail())
                .fullname(form.getName())
                .password(encoder.encode(form.getPassword()))
                .build();

        repository.save(user);

        return CustomerResponseDTO.from(user);
    }
    public Page<CustomerDTO> getCustomers(Pageable pageable){
        var users = repository.findAll(pageable);
        return users.map(CustomerDTO::from);

    }
}
