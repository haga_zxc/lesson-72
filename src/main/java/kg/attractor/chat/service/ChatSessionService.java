package kg.attractor.chat.service;

import kg.attractor.chat.DTO.ChatSessionDTO;
import kg.attractor.chat.model.ChatSession;
import kg.attractor.chat.model.Customer;
import kg.attractor.chat.model.CustomerRegisterForm;
import kg.attractor.chat.model.Message;
import kg.attractor.chat.repository.ChatSessionRepository;
import kg.attractor.chat.repository.CustomerRepository;
import kg.attractor.chat.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.List;

@Service
@AllArgsConstructor
public class ChatSessionService {
    private final ChatSessionRepository csr;
    private final CustomerRepository cr;
    private final MessageRepository mr;

    public void addMessage(String text, String session, Authentication authentication){
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var user = cr.findByEmail(userDetails.getUsername());
        var message = Message.builder()
                .text(text)
                .customer(user)
                .build();
        mr.save(message);

        var chatSession = ChatSession.builder()
                .session(session)
                .customer(user)
                .message(message)
                .date(new Timestamp(System.currentTimeMillis()))
                .build();
        csr.save(chatSession);
    }
    public Page<ChatSessionDTO> getSession(Pageable pageable,String id,Authentication authentication){
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var user = cr.findByEmail(userDetails.getUsername());
        var messages = csr.findAll(pageable);
        return messages.map(ChatSessionDTO::from);
    }
    public Message getMessage(int id){
        List<Message> messages = mr.findAllById(id);
        if(messages!=null){
            for(Message m: messages){
                if(!m.isDelivered()){
                    m.setDelivered(true);
                    mr.save(m);
                    return m;
                }
            }
        }
        return null;
    }
}
