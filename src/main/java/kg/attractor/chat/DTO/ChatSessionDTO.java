package kg.attractor.chat.DTO;

import kg.attractor.chat.model.ChatSession;
import lombok.*;

import java.sql.Timestamp;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(access = AccessLevel.PACKAGE)
@ToString
public class ChatSessionDTO {
    private int id;
    private String session;
    private CustomerDTO customer;
    private MessageDTO message;
    private Timestamp date;

    public static ChatSessionDTO from (ChatSession chatSession){
        return builder()
                .customer(CustomerDTO.from(chatSession.getCustomer()))
                .message(MessageDTO.from(chatSession.getMessage()))
                .id(chatSession.getId())
                .session(chatSession.getSession())
                .date(chatSession.getDate())
                .build();
    }
}
