package kg.attractor.chat.DTO;


import kg.attractor.chat.model.Message;
import lombok.*;


@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(access = AccessLevel.PACKAGE)
@ToString
public class MessageDTO {
    private int id;
    private String text;
    private CustomerDTO customer;

    public static MessageDTO from(Message message){
        return builder()
                .customer(CustomerDTO.from((message.getCustomer())))
                .id(message.getId())
                .text(message.getText())
                .build();
    }


}
