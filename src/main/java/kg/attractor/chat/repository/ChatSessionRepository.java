package kg.attractor.chat.repository;

import kg.attractor.chat.model.ChatSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ChatSessionRepository extends JpaRepository<ChatSession, Integer> {

    Page<ChatSession> findAll(Pageable pageable);

}
