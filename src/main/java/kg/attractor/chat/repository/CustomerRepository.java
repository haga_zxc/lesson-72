package kg.attractor.chat.repository;

import kg.attractor.chat.model.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    boolean existsByEmail(String email);
    Customer findByEmail(String email);
    Page<Customer> findAll(Pageable pageable);

}
