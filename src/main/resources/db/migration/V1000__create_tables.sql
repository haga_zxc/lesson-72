use `chat`;

CREATE TABLE `customers` (
     `id` int auto_increment NOT NULL,
     `email` varchar(128) NOT NULL,
     `password` varchar(128) NOT NULL,
     `fullname` varchar(128) NOT NULL default ' ',
     `enabled` boolean NOT NULL default true,
     `role` varchar(16) NOT NULL default 'USER',
     PRIMARY KEY (`id`),
     UNIQUE INDEX `email_unique` (`email` ASC)
);
CREATE TABLE `messages` (
     `id` int auto_increment NOT NULL,
     `text` varchar(600) NOT NULL,
     `customers_id` int(11) NOT NULL,
     `delivered` boolean NOT NULL default false,
     PRIMARY KEY (`id`),
     CONSTRAINT `fk_messages`
         FOREIGN KEY (`customers_id`) REFERENCES `customers` (`id`)
);

CREATE TABLE `chat_session`(
    `id`           INT          NOT NULL AUTO_INCREMENT,
    `messages_id`  INT(11)      null,
    `customers_id` INT(11)      null,
    `session` varchar(50) NOT NULL,
    `date`         datetime     NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_chat-session1`
        FOREIGN KEY (`messages_id`) REFERENCES `messages` (`id`),
    CONSTRAINT `fk_chat-session`
        FOREIGN KEY (`customers_id`) REFERENCES `customers` (`id`)
);